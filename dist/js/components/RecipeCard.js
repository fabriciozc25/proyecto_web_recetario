app.component("recipe-card", {
  props: {
    image: {
      type: String,
    },
    category: {
      type: String,
      default: "default category",
    },
    name: {
      type: String,
      default: "default name",
    },
    likes: {
      type: Number,
      default: 0,
    },
    level: {
      type: String,
      default: "default level",
    },
    time: {
      type: String,
      default: "default time",
    },
    index: {
      type: Number,
    },
  },
  methods: {
    onClickLike() {
      this.$emit("recipelike");
    },
    onClickViewRecipe() {
      this.$emit("recipedetails", this.index);
    },
  },
  template: /*html*/ `
  <div class="card-recipe">
  <div class="position-relative" style="height: 100%;">
    <a href="#" v-on:click.prevent="onClickViewRecipe()" data-bs-toggle="modal"
    data-bs-target="#staticBackdrop">
      <img v-bind:src="image" class="img-fluid img-recipe" alt="Imagen" style="height: 100%; width: 100%;">
    </a>
    <div class="position-absolute bottom-0 end-0 p-3">
      <span class="like-recipe ms-2 fa-regular fa-heart" v-on:click="onClickLike()" style="cursor: pointer">
        <i></i>  {{ likes }}
      </span>
    </div>
  </div>
  <div class="row align-items-end">
    <div class="card-body col-lg-6 col-md-6 col-7" style="padding-left: 2.1rem;">
      <p class="card-category">{{ category }}</p>
      <h4 class="card-name">
        <a href="#" v-on:click.prevent="onClickViewRecipe()" data-bs-toggle="modal"
        data-bs-target="#staticBackdrop" class="a-title">{{ name }}</a>
      </h4>
      <p class="card-text" style="margin-bottom: -0.05rem; margin-top: rem;">{{ time }}</p>
      <p class="card-text pt-2">{{ level }}</p>
    </div>
  </div>
</div>

  `,
});
