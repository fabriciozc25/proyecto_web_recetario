app.component("register", {
  data() {
    return {
      name: "",
      username: "",
      email: "",
      password: "",
    };
  },
  methods: {
    register() {
      console.log("Nombre:", this.name);
      console.log("Usuario:", this.username);
      console.log("Correo:", this.email);
      console.log("Contraseña:", this.password);

      this.name = "";
      this.username = "";
      this.email = "";
      this.password = "";
    },
  },
  template: /*html*/ `
    <form action="#" @submit.prevent="register">
      <div class="mb-4">
        <label for="name" class="form-label">Nombre completo</label>
        <input type="text" class="form-control" name="name" v-model="name">
      </div>
      <div class="mb-4">
        <label for="user" class="form-label">Usuario</label>
        <input type="text" class="form-control" name="username" v-model="username">
      </div>
      <div class="mb-4">
        <label for="email" class="form-label">Correo Electrónico</label>
        <input type="text" class="form-control" name="email" v-model="email">
      </div>
      <div class="mb-4">
        <label for="password" class="form-label">Contraseña</label>
        <input type="password" class="form-control" name="password" v-model="password">
      </div>
      <div class="d-grid">
        <button type="submit" class="btn-form py-1">Continuar</button>
      </div>
      <div class="my-3 mb-lg-1">
        <span>Ya tienes cuenta? <a href="./log_in.html">Iniciar sesión</a></span><br>
      </div>
    </form>
  `,
});

  