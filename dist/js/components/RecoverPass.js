app.component("recover-pass", {
  data() {
    return {
      email: "",
    };
  },
  methods: {
    submitEmail() {
    
      console.log("Correo Electrónico:", this.email);
 
      this.email = "";
    },
  },
  template:
    /*html*/
    `<form action="#" @submit.prevent="submitEmail">
    <div class="mb-4">
      <label for="user" class="form-label">Correo Electrónico</label>
      <input type="text" class="form-control" name="user" v-model="email">
    </div>
    <div class="d-grid my-5">
      <button type="submit" class="btn-form py-1">Enviar</button>
    </div>
  </form>
  `,
});
