app.component("login", {
  data() {
    return {
      username: "",
      password: "",
    };
  },
  methods: {
    logIn() {
      console.log("Usuario:", this.username);
      console.log("Contraseña:", this.password);

      this.username = "";
      this.password = "";
    },
  },
  template: /*html*/ `
    <form action="#" @submit.prevent="logIn">
      <div class="mb-4">
        <label for="user" class="form-label">Usuario</label>
        <input type="text" class="form-control" name="user" v-model="username">
      </div>
      <div class="mb-4">
        <label for="password" class="form-label">Contraseña</label>
        <input type="password" class="form-control" name="password" v-model="password">
      </div>
      <div class="d-grid">
        <button type="submit" class="btn-form py-1">Iniciar Sesión</button>
      </div>
      <div class="my-3 mb-lg-5">
        <span>No tienes cuenta? <a href="./register.html">Registrate</a></span><br>
        <span><a href="./recover_pass.html">Recuperar Contraseña</a></span>
      </div>
    </form>
  `,
});
