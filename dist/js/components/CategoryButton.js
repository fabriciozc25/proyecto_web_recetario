app.component("category-button", {
    template:
    /*html*/
     `<div class="col-md-12">
     <div class="d-flex flex-nowrap overflow-auto" style="max-width: 100%">
         <ul class="list-group list-group-horizontal mx-lg-auto">
             <li class="list-group-item border-0 py-3">
                 <a class="filter">Todo</a>
             </li>
             <li class="list-group-item border-0 py-3">
                 <a class="filter">Desayuno</a>
             </li>
             <li class="list-group-item border-0 py-3">
                 <a class="filter">Bebidas</a>
             </li>
             <li class="list-group-item border-0 py-3">
                 <a class="filter">Entradas</a>
             </li>
             <li class="list-group-item border-0 py-3">
                 <a class="filter">Almuerzo</a>
             </li>
             <li class="list-group-item border-0 py-3">
                 <a class="filter">Postres</a>
             </li>
             <li class="list-group-item border-0 py-3">
                 <a class="filter">Sopas</a>
             </li>
         </ul>
     </div>
 </div>
    `,
  });
  