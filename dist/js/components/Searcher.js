app.component("searcher", {
  methods: {
    searchRecipes() {
      const valor = this.$refs.searchInput.value; // Obtener el valor de la búsqueda

      axios
        .get(
          `https://api.spoonacular.com/recipes/complexSearch?query=${valor}&apiKey=44928ee20c7549f48373e1f1aa8bf90e`
        )
        .then((response) => {
          const results = response.data.results;

          this.$emit("recipesfound", results);
        })
        .catch((error) => {
          console.log(error);
        });
    },
  },
  template: /*html*/ `
    <form>
      <div class="input-group">
        <input ref="searchInput" type="search" class="form-control search rounded-start-5 pl-4" placeholder="Buscar receta">
        <button type="button" class="btn-search" v-on:click="searchRecipes"><i class="fa-solid fa-magnifying-glass"></i></button>
      </div>
    </form>
  `,
});
