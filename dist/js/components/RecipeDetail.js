app.component("recipe-detail", {
  props: {
    image: {
      type: String,
    },
    name: {
      type: String,
      default: "default name",
    },
    likes: {
      type: Number,
      default: 0,
    },
    category: {
      type: String,
      default: "default category",
    },
    occasion: {
      type: String,
      default: "default occasion",
    },
    level: {
      type: String,
      default: "default level",
    },
    portions: {
      type: String,
      default: "default portions",
    },
    description: {
      type: String,
      default: "default description",
    },
    ingredients: {
      type: String,
      default: "default ingredients",
    },
    preparation_time: {
      type: String,
      default: "default preparationTime",
    },
    cooking_time: {
      type: String,
      default: "default cookingTime",
    },
    total_time: {
      type: String,
      default: "default totalTime",
    },
    instructions: {
      type: String,
      default: "default instructions",
    },
    index: {
      type: Number,
    },
  },
  template: /*html*/ `

  <div class="position-relative">
                                <button type="button" class="btn-close float-end" data-bs-dismiss="modal"
                                    aria-label="Close"></button>
                            </div>
                            <div class="row pt-3">
                                <div class="col-md-6 text-center mt-lg-5">
                                    <h2 class="recipe-title mb-4">{{ name }}</h2>
                                    <div class="position-relative">
                                        <img :src="image" class="img-fluid img-recipe" alt="Imagen Receta">
                                        <div class="position-absolute bottom-0 end-0 p-3">
                                            <a href="#">
                                                <span class="like-recipe ms-2 fa-regular fa-heart">{{ likes }}</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 pt-4">
                                    <div class="row justify-content-between icon-text">
                                        <div class="col text-center">
                                            <i class="fa-solid fa-chart-simple"></i>
                                            <br>
                                            <span>{{ level }}</span>
                                        </div>
                                        <div class="col text-center">
                                            <i class="fa-solid fa-bowl-food"></i>
                                            <br>
                                            <span>{{ category }}</span>
                                        </div>
                                        <div class="col text-center">
                                            <i class="fa-solid fa-utensils"></i>
                                            <br>
                                            <span>{{ occasion }}</span>
                                        </div>
                                        <div class="col text-center">
                                            <i class="fa-solid fa-pizza-slice"></i>
                                            <br>
                                            <span>{{ portions }}</span>
                                        </div>
                                    </div>
                                    <div class="col-11 mx-auto pt-5">
                                        <div class="mb-5">
                                            <h3 class="pb-2 recipe-subtitle">Descripción</h3>
                                            <p>{{ description }}</p>
                                        </div>
                                        <div class="mb-5">
                                            <h3 class="pb-2 recipe-subtitle">Ingredientes</h3>
                                            <p>{{ ingredients }}</p>
                                        </div>
                                        <div class="row justify-content-between pb-5">
                                            <div class="col text-center">
                                                <p class="fw-bold">Tiempo de Preparación</p>
                                                <p>{{ preparation_time }}</p>
                                            </div>
                                            <div class="col text-center">
                                                <p class="fw-bold">Tiempo de Cocción</p>
                                                <p>{{ cooking_time }}</p>
                                            </div>
                                            <div class="col text-center">
                                                <p class="fw-bold">Tiempo Total</p>
                                                <p>{{ total_time }}</p>
                                            </div>
                                        </div>
                                        <div class="mb-5">
                                            <h3 class="pb-2 recipe-subtitle">Instrucciones</h3>
                                            <p>{{ instructions }}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

  `,
});
