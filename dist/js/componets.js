const app = Vue.createApp({
  data() {
    return {
      recipe: {},
      recipes: [],
      topRecipes: [],
      myRecipes: [],
    };
  },
  mounted: function () {
    axios({
      method: "get",
      url: "https://api.spoonacular.com/recipes/complexSearch?type=bread&apiKey=44928ee20c7549f48373e1f1aa8bf90e",
    })
      .then((response) => {
        let items = response.data.results;

        items.forEach((element) => {
          this.recipes.push({
            id: element.id,
            image: element.image,
            name: element.title,
            category: "Bread",
            time: "20 mins",
            level: "Easy",
            likes: 0,
          });
        });

        items.forEach((element) => {
          this.topRecipes.push({
            image: element.image,
            name: element.title,
            category: "Bread",
            time: "20 mins",
            level: "Easy",
            likes: 0,
          });
        });
      })
      .catch((error) => console.log(error));
  },
  methods: {
    onClickRecipeLike(index) {
      const currentLikes = this.recipes[index].likes;

      if (currentLikes % 2 === 0) {
        this.recipes[index].likes = currentLikes + 1;
        this.myRecipes.push(this.recipes[index]);
      } else {
        this.recipes[index].likes = currentLikes - 1;
      }

      console.log("misrecetas:", this.myRecipes);
    },
    handleRecipesFound(results) {
      this.recipes = results.map((element) => ({
        image: element.image,
        name: element.title,
        category: "Bread",
        time: "20 mins",
        level: "Easy",
        likes: 0,
      }));
    },
    onClickRecipeDetails(index) {
      console.log(index);

      axios({
        method: "get",
        url:
          "https://api.spoonacular.com/recipes/" +
          index +
          "/information?includeNutrition=false&apiKey=44928ee20c7549f48373e1f1aa8bf90e",
      })
        .then((response) => {
          let item = response.data;
          console.log(item);

          this.recipe.id = index;
          this.recipe.image = item.image;
          this.recipe.name = item.title;
          this.recipe.category = item.dishTypes[0];
          this.recipe.preparation_time = "10 mins";
          this.recipe.cooking_time = "35 mins";
          this.recipe.total_time = item.readyInMinutes + " mins";
          this.recipe.level = "Easy";
          this.recipe.occasion = "Semana santa";
          this.recipe.likes = item.aggregateLikes;
          this.recipe.instructions = item.instructions;
          this.recipe.portions = item.servings;
          this.recipe.description = "Descripción general";

          let ingredientsList = "";
          for (let i = 0; i < item.extendedIngredients.length; i++) {
            ingredientsList += item.extendedIngredients[i].original + "\n";
          }

          this.recipe.ingredients = ingredientsList;

          console.log(this.recipe);
 
        })
        .catch((error) => console.log(error));
    },
  },
});
